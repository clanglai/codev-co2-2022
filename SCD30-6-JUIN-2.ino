// Code pour faire fonctionner capteur et les 3 leds ensemble avec les conditions
#include <Adafruit_SCD30.h>
#include <SPI.h>
#include <SD.h>
#include <Wire.h>
#include <SPI.h>

Adafruit_SCD30  scd30;

File myFile;
const int L1 = 7; //rouge
const int L2 = 3; //jaune
const int L3 =2 ; //vert

void save_co2() {
  myFile = SD.open("CO2.txt", FILE_WRITE);

  myFile.println(scd30.CO2);
  
  myFile.close();
}

void setup(void) {
  
  pinMode(L1,OUTPUT);
  pinMode(L2,OUTPUT);
  pinMode(L3,OUTPUT);

  Serial.begin(115200);
  while (!Serial) delay(10); // will pause Zero, Leonardo, etc until serial console opens

  Serial.println("Adafruit SCD30 test!");

  // Try to initialize!
  if (!scd30.begin()) {
    Serial.println("Failed to find SCD30 chip");
    while (1) { delay(10); }
  }
  Serial.println("SCD30 Found!");


  // if (!scd30.setMeasurementInterval(10)){
  //   Serial.println("Failed to set measurement interval");
  //   while(1){ delay(10);}
  // }
  Serial.print("Measurement Interval: "); 
  Serial.print(scd30.getMeasurementInterval()); 
  Serial.println(" seconds");

  // Initialisation de la carte SD
  Serial.print("Initializing SD card...");
  if (!SD.begin(10)) {
    Serial.println("initialization failed!");
    digitalWrite(L1,HIGH);
    delay(200);
    digitalWrite(L1,LOW);
    delay(200);
    digitalWrite(L1,HIGH);
    delay(200);
    digitalWrite(L1,LOW);
    delay(200);
    digitalWrite(L1,HIGH);
    delay(200);
    digitalWrite(L1,LOW);
    delay(200);
    digitalWrite(L1,HIGH);
    delay(200);
    digitalWrite(L1,LOW);
    delay(200);
    digitalWrite(L1,HIGH);
    delay(200);
    digitalWrite(L1,LOW);
    delay(200);
    digitalWrite(L1,HIGH);
    delay(200);
    digitalWrite(L1,LOW);
    while (1);
  }
  Serial.println("initialization done.");
  digitalWrite(L3,HIGH);
  delay(200);
  digitalWrite(L3,LOW);
  delay(200);
  digitalWrite(L3,HIGH);
  delay(200);
  digitalWrite(L3,LOW);
  delay(200);
  digitalWrite(L3,HIGH);
  delay(200);
  digitalWrite(L3,LOW);
  delay(200);
  digitalWrite(L3,HIGH);
  delay(200);
  digitalWrite(L3,LOW);
  delay(200);
  digitalWrite(L3,HIGH);
  delay(200);
  digitalWrite(L3,LOW);
  delay(200);
  digitalWrite(L3,HIGH);
  delay(200);
  digitalWrite(L3,LOW);

  myFile = SD.open("CO2.txt", FILE_WRITE);
  myFile.println(" ");
  myFile.println("----------- NOUVELLE MESURE -------------");
  myFile.close();
  
}

void loop() {
  if (scd30.dataReady()){
    Serial.println("Data available!");

    if (!scd30.read()){ Serial.println("Error reading sensor data"); return; }

    Serial.print("Temperature: ");
    Serial.print(scd30.temperature);
    Serial.println(" degrees C");
    
    Serial.print("Relative Humidity: ");
    Serial.print(scd30.relative_humidity);
    Serial.println(" %");
    
    Serial.print("CO2: ");
    Serial.print(scd30.CO2, 3);
    Serial.println(" ppm");
    Serial.println("");

    save_co2();

  } else {
    //Serial.println("No data");
  }

  delay(100);

  //programmation des LEDS

  if (scd30.CO2 > 1000){ //rouge
    digitalWrite(L1,HIGH);
    digitalWrite(L2,LOW);
    digitalWrite(L3,LOW);
  }
  else if(scd30.CO2 < 800 and scd30.CO2 > 300) { //vert
    digitalWrite(L1,LOW);
    digitalWrite(L2,LOW);
    digitalWrite(L3,HIGH); 
  }

  else if(scd30.CO2 <300) { //stabilisation en attendant la calibration du capteur
    digitalWrite(L1,LOW);
    digitalWrite(L2,LOW);
    digitalWrite(L3,LOW);
    delay(2000);
  }
  else { //jaune
     digitalWrite(L1,LOW);
    digitalWrite(L2,HIGH);
    digitalWrite(L3,LOW);
  }
  
}
