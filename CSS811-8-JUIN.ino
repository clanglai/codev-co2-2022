#include "Adafruit_CCS811.h"
#include <SPI.h>
#include <SD.h>
#include <Wire.h>

Adafruit_CCS811 ccs;
File myFile;
Sd2Card card;

void save_co2() {
  myFile = SD.open("CO2.txt", FILE_WRITE);

  myFile.println(ccs.geteCO2());
  
  myFile.close();
}

void setup() {

   pinMode(7,OUTPUT); //règle la borne numérique numéro 1 de la carte Arduino en mode sortie
   pinMode(2,OUTPUT);
   pinMode(3,OUTPUT);

   
  Serial.begin(115200);
  while (!Serial) delay(10); // will pause Zero, Leonardo, etc until serial console opens
  
 // Initialisation de la carte SD
  Serial.print("Initializing SD card...");
  if (!SD.begin(4)) {
    Serial.println("initialization failed!");
    digitalWrite(3,HIGH);
    delay(200);
    digitalWrite(3,LOW);
    delay(200);
    digitalWrite(3,HIGH);
    delay(200);
    digitalWrite(3,LOW);
    delay(200);
    digitalWrite(3,HIGH);
    delay(200);
    digitalWrite(3,LOW);
    delay(200);
    digitalWrite(3,HIGH);
    delay(200);
    digitalWrite(3,LOW);
    delay(200);
    digitalWrite(3,HIGH);
    delay(200);
    digitalWrite(3,LOW);
    delay(200);
    digitalWrite(3,HIGH);
    delay(200);
    while (1);
  }
  Serial.println("initialization done.");
  digitalWrite(7,HIGH);
    delay(200);
    digitalWrite(7,LOW);
    delay(200);
    digitalWrite(7,HIGH);
    delay(200);
    digitalWrite(7,LOW);
    delay(200);
    digitalWrite(7,HIGH);
    delay(200);
    digitalWrite(7,LOW);
    delay(200);
    digitalWrite(7,HIGH);
    delay(200);
    digitalWrite(7,LOW);
    delay(200);
    digitalWrite(7,HIGH);
    delay(200);
    digitalWrite(7,LOW);
    delay(200);
    digitalWrite(7,HIGH);
    delay(200);
    digitalWrite(7,LOW);

    Serial.println("CCS811 test");
    if(!ccs.begin()){
      Serial.println("Failed to start sensor! Please check your wiring.");
      digitalWrite(3,HIGH);
      while(1);
    }

    Serial.println("C'est sensé écrire qqch");
    myFile = SD.open("CO2.txt", FILE_WRITE);
    myFile.println(" ");
    myFile.println("-------------------------");
    myFile.close();
}

void loop() {
  Serial.print("CO2: ");
  Serial.print(ccs.geteCO2());
  
  if(ccs.available()){
      Serial.print("CO2: ");
      Serial.print(ccs.geteCO2());
    if(!ccs.readData()){
      Serial.print("CO2: ");
      Serial.print(ccs.geteCO2());
    }
   }

  if (ccs.geteCO2() > 1000 ){
    digitalWrite(7,LOW); //le courant est envoyé sur la borne 1, la LED  s'allume
    digitalWrite(2,LOW); //le courant est envoyé sur la borne 2, la LED  s'allume
    digitalWrite(3,HIGH); //le courant est envoyé sur la borne 3, la LED  s'allume
  }
  
  else if (ccs.geteCO2() > 800 and ccs.geteCO2() < 1000){
    digitalWrite(7,LOW); //le courant est envoyé sur la borne 1, la LED  s'allume
    digitalWrite(2,HIGH); //le courant est envoyé sur la borne 2, la LED  s'allume
    digitalWrite(3,LOW); //le courant est envoyé sur la borne 3, la LED  s'allume
  }

  else if (ccs.geteCO2() < 800){
    digitalWrite(7,HIGH); //le courant est envoyé sur la borne 1, la LED  s'allume
    digitalWrite(2,LOW); //le courant est envoyé sur la borne 2, la LED  s'allume
    digitalWrite(3,LOW); //le courant est envoyé sur la borne 3, la LED  s'allume
  }
  
 save_co2();
delay(500);
}
